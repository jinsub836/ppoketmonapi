package com.example.ppoketmonapi.entity;

import com.example.ppoketmonapi.interfaces.CommonModelBuilder;
import com.example.ppoketmonapi.model.ScoreRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long classNo;

    private String name;

    private Long math;

    private Long english;

    private Long science;
}