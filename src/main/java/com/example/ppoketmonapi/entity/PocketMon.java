package com.example.ppoketmonapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class PocketMon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String pocketImg;

    private String pocketName;

    private String pocketType;

    private Integer hp;

    private Integer speed;

    private Integer offensive;

    private Integer defensive;

    private Integer evolutionGrade;

    private String etcMemo;
}
