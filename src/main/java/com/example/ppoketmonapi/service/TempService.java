package com.example.ppoketmonapi.service;

import com.example.ppoketmonapi.entity.PocketMon;
import com.example.ppoketmonapi.lib.CommonFile;
import com.example.ppoketmonapi.repository.PocketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class TempService {
    private final PocketRepository pocketRepository;
    public void setRentCarFile(MultipartFile multipartFile) throws IOException {
        // file 받아온 파일을 컨버팅 함
        File file = CommonFile.multipartToFile(multipartFile);
        //파싱
       BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        System.out.println(file);
        System.out.println(bufferedReader);
        String line = "";
        int index = 0;
            //한 라인 읽기
        while ((line= bufferedReader.readLine()) != null ){
            if(index >1){
                //한 라인을 받은 데이터 한칸 씩 나누기
             String[] cols = line.split(",");
                System.out.println(cols[1]);
                System.out.println(cols[2]);
                System.out.println(cols[3]);
                System.out.println(Integer.parseInt(cols[4]));
                System.out.println(cols[5]);
                System.out.println(cols[6]);
                System.out.println(cols[7]);
                System.out.println(cols[8]);
                System.out.println(cols[9]);
    /*            if(cols.length == 10){
                    PocketMon addData = new PocketMon();
                    addData.setPocketImg(cols[1]);
                    addData.setPocketName(cols[2]);
                    addData.setPocketType(cols[3]);
                    addData.setHp(Integer.parseInt(cols[4]));
                    addData.setSpeed(Integer.parseInt(cols[5]));
                    addData.setOffensive(Integer.parseInt(cols[6]));
                    addData.setDefensive(Integer.parseInt(cols[7]));
                    addData.setEvolutionGrade(Integer.parseInt(cols[8]));
                    addData.setEtcMemo(cols[9]);

                    pocketRepository.save(addData);
                }*/
            System.out.println(index);
            }
            index++;
        }
        bufferedReader.close();
    }
}


