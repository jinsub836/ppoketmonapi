package com.example.ppoketmonapi.service;

import com.example.ppoketmonapi.entity.BattleField;
import com.example.ppoketmonapi.entity.PocketMon;
import com.example.ppoketmonapi.exception.CMonsterFullException;
import com.example.ppoketmonapi.model.Battle.BattleMonsterItem;
import com.example.ppoketmonapi.model.Battle.BattleResponse;
import com.example.ppoketmonapi.repository.BattleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BattleService {
    final BattleRepository battleRepository;

    public void setBattle(PocketMon pocketMon) throws CMonsterFullException {
        List<BattleField> battlelist = battleRepository.findAll();
        //필드에 몬스터가 2마리 이상이면 안되므로 리스트를 부르고 사이즈로 제한
        if(battlelist.size() >= 2) throw new CMonsterFullException();

        BattleField battleField = new BattleField();
        battleField.setPocketId(pocketMon);
        battleRepository.save(battleField);
    }

    public void delMonster (long id){
        battleRepository.deleteById(id);
    }

    public BattleResponse getBattleMonsters (){
        List<BattleField> battleFieldList = battleRepository.findAll();
        BattleResponse result = new BattleResponse();
        //만약 2마리 일때 2마리의 값을 넣어줘야됨
       result.setMonster1(convert(battleFieldList.get(0)));

        if(battleFieldList.size() == 2 ){
            result.setMonster1(convert(battleFieldList.get(0)));
            result.setMonster2(convert(battleFieldList.get(1)));

        } else if (battleFieldList.size()==1){
            result.setMonster1(convert(battleFieldList.get(0)));
            } return result;
    }

    //중복 코드를 방지하기 위해 리펙터링 진행
    private BattleMonsterItem convert (BattleField battleField){
       BattleMonsterItem battleMonsterItem = new BattleMonsterItem();
        battleMonsterItem.setMonsterBattleFieldId(battleField.getId());
        battleMonsterItem.setMonsterId(battleField.getPocketId().getId());
        battleMonsterItem.setPocketImg(battleField.getPocketId().getPocketImg());
        battleMonsterItem.setMonsterName(battleField.getPocketId().getPocketName());
        battleMonsterItem.setMonsterType(battleField.getPocketId().getPocketType());
        battleMonsterItem.setMonsterHp(battleField.getPocketId().getHp());
        battleMonsterItem.setMonsterSpeed(battleField.getPocketId().getSpeed());
        battleMonsterItem.setMonsterOffensive(battleField.getPocketId().getOffensive());
        battleMonsterItem.setMonsterDefensive(battleField.getPocketId().getDefensive());

        return battleMonsterItem;
    }

}
