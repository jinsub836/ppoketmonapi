package com.example.ppoketmonapi.service;

import com.example.ppoketmonapi.entity.PocketMon;
import com.example.ppoketmonapi.model.PocketItem;
import com.example.ppoketmonapi.model.PocketRequest;
import com.example.ppoketmonapi.model.PocketResponse;
import com.example.ppoketmonapi.repository.PocketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PocketService {
    private final PocketRepository pocketRepository;

    public PocketMon getData(long id){ return pocketRepository.findById(id).orElseThrow();}

    public void setPocketMon(PocketRequest request){
        PocketMon pocketItem = new PocketMon();

        pocketItem.setPocketImg(request.getPocketImg());
        pocketItem.setPocketName(request.getPocketName());
        pocketItem.setPocketType(request.getPocketType());
        pocketItem.setHp(request.getHp());
        pocketItem.setSpeed(request.getSpeed());
        pocketItem.setOffensive(request.getOffensive());
        pocketItem.setDefensive(request.getDefensive());
        pocketItem.setEvolutionGrade(request.getEvolutionGrade());
        pocketItem.setEtcMemo(request.getEtcMemo());

        pocketRepository.save(pocketItem);


    }


    public List<PocketItem> getPocketMon(){
        List<PocketMon> originlist = pocketRepository.findAll();
        List<PocketItem> result = new LinkedList<>();
        for (PocketMon pocketMon: originlist ){
            PocketItem addItem = new PocketItem();
            addItem.setId(pocketMon.getId());
            addItem.setPocketImg(pocketMon.getPocketImg());
            addItem.setPocketName(pocketMon.getPocketName());
            addItem.setPocketType(pocketMon.getPocketType());
            addItem.setOffensive(pocketMon.getOffensive());
            addItem.setDefensive(pocketMon.getDefensive());

            result.add(addItem);
        }
        return result;
    }

    public PocketResponse getPocketMons(long id){
        PocketMon originData = pocketRepository.findById(id).orElseThrow();
        PocketResponse response = new PocketResponse();
        response.setPocketImg(originData.getPocketImg());
        response.setPocketName(originData.getPocketName());
        response.setPocketType(originData.getPocketType());
        response.setHp(originData.getHp());
        response.setSpeed(originData.getSpeed());
        response.setOffensive(originData.getOffensive());
        response.setDefensive(originData.getDefensive());
        response.setEvolutionGrade(originData.getEvolutionGrade());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putPocketMon(long id){
        PocketMon originData = pocketRepository.findById(id).orElseThrow();

        String del = "del";

        originData.setPocketName(originData.getPocketName()+del);

        pocketRepository.save(originData);

    }
}
