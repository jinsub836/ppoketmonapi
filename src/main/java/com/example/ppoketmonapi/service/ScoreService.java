package com.example.ppoketmonapi.service;

import com.example.ppoketmonapi.entity.Score;
import com.example.ppoketmonapi.model.ScoreItem;
import com.example.ppoketmonapi.repository.ScoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ScoreService {
    private final ScoreRepository scoreRepository;

    public List<ScoreItem> getClassAVG(int claasNo){
        List<ScoreItem> result = new LinkedList<>();
        for (int i=1; i<(claasNo+1); i++){
            long math = 0; long english = 0; long science = 0;
            List<Score> originlist = scoreRepository.findByClassNo(i);
            for (Score score : originlist) {
                math += score.getMath() / originlist.size();
                english += score.getEnglish() / originlist.size();
                science += score.getScience() / originlist.size();}
            ScoreItem scoreItem = new ScoreItem();
            scoreItem.setClassNo(i);
            scoreItem.setEnglishScore(english);
            scoreItem.setMathScore(math);
            scoreItem.setScienceScore(science);
            result.add(scoreItem);}
        return result;
    }
}
