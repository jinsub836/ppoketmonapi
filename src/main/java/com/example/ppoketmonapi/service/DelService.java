package com.example.ppoketmonapi.service;

import com.example.ppoketmonapi.entity.BattleField;
import com.example.ppoketmonapi.model.Img;
import com.example.ppoketmonapi.repository.BattleRepository;
import com.example.ppoketmonapi.repository.PocketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DelService {
    private final BattleRepository battleRepository;
    private final PocketRepository pocketRepository;

    public void delData (BattleField battleField){
        battleRepository.deleteById(battleField.getId());
        pocketRepository.deleteById(battleField.getPocketId().getId());
    }

    public void setImg(){
        Img img1 = new Img();
        img1.setImg("img1.jpg");
        Img img2 = new Img();
        img2.setImg("img2.jpg");
        Img img3 = new Img();
        img3.setImg("img3.jpg");
        Img img4 = new Img();
        img4.setImg("img4.jpg");
    }
}
