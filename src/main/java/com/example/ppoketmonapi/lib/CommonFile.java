package com.example.ppoketmonapi.lib;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class CommonFile {
    public static File multipartToFile(MultipartFile multipartFile) throws IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+multipartFile.getOriginalFilename());
        // convFile 은 multipart 파일을 파일로 변환 하는 경로를 지정 해준다.
        multipartFile.transferTo(convFile);
        //multipartFile 을 받아서 convFile 로 변환 한다.
        return convFile;
        //변환된 파일을 return 으로 받는다.
    }
}
