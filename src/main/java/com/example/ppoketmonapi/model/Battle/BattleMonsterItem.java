package com.example.ppoketmonapi.model.Battle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleMonsterItem {
    private Long monsterBattleFieldId;
    private Long monsterId;
    private String pocketImg;
    private String monsterName;
    private String monsterType;
    private Integer monsterHp;
    private Integer monsterSpeed;
    private Integer monsterOffensive;
    private Integer monsterDefensive;
}
