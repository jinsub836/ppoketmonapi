package com.example.ppoketmonapi.model.Battle;

import com.example.ppoketmonapi.model.CommonResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleResponse extends CommonResult {
    private BattleMonsterItem monster1;
    private BattleMonsterItem monster2;
}
