package com.example.ppoketmonapi.model.Battle;

import com.example.ppoketmonapi.entity.PocketMon;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BattleRequest {

    private PocketMon pocketId;
}
