package com.example.ppoketmonapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreItem {
    private  Integer classNo;
    private  Long mathScore;
    private  Long englishScore;
    private  Long scienceScore;
}
