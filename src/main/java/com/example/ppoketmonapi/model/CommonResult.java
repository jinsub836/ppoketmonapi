package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonResult {
   private String msg;
   private Integer code;
}
