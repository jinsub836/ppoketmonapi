package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreRequest extends CommonResult {
    private  Long classNo;
    private  String name;
    private  Long mathScore;
    private  Long englishScore;
    private  Long scienceScore;
}
