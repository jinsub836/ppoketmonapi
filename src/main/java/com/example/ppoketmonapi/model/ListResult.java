package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private String msg;

    private List<T> list;

    private Integer totalCount;
}
