package com.example.ppoketmonapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PocketItem {
    private Long id;

    private String pocketImg;

    private String pocketName;

    private String pocketType;

    private Integer offensive;

    private Integer defensive;

}
