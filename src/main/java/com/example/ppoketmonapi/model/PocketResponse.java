package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PocketResponse {
    private String pocketImg;
    private String pocketName;
    private String pocketType;
    private Integer hp;
    private Integer speed;
    private Integer offensive;
    private Integer defensive;
    private Integer evolutionGrade;
    private String etcMemo;
}
