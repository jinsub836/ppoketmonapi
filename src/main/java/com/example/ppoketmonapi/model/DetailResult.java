package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailResult<T> extends CommonResult{
    private T data;
}
