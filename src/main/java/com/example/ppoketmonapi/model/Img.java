package com.example.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Img {
    private String img;
}
