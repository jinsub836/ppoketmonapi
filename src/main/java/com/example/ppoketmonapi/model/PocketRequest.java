package com.example.ppoketmonapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PocketRequest extends CommonResult {
    private String pocketImg;
    private String pocketName;
    private String pocketType;
    private Integer hp;
    private Integer speed;
    private Integer offensive;
    private Integer defensive;
    private Integer evolutionGrade;
    private String etcMemo;
}
