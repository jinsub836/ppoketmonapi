package com.example.ppoketmonapi.repository;

import com.example.ppoketmonapi.entity.PocketMon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PocketRepository extends JpaRepository<PocketMon, Long> {
    List<PocketMon> findAllByOrderByIdAsc();
}
