package com.example.ppoketmonapi.repository;

import com.example.ppoketmonapi.entity.Score;
import com.example.ppoketmonapi.model.ScoreItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScoreRepository extends JpaRepository<Score,Long> {
        List<Score> findByClassNo(int no);
}
