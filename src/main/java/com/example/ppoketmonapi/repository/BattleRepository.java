package com.example.ppoketmonapi.repository;

import com.example.ppoketmonapi.entity.BattleField;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleRepository extends JpaRepository<BattleField,Long> {
}
