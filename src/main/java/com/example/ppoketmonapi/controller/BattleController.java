package com.example.ppoketmonapi.controller;

import com.example.ppoketmonapi.entity.BattleField;
import com.example.ppoketmonapi.entity.PocketMon;
import com.example.ppoketmonapi.model.Battle.BattleRequest;
import com.example.ppoketmonapi.model.Battle.BattleResponse;
import com.example.ppoketmonapi.model.CommonResult;
import com.example.ppoketmonapi.model.ScoreItem;
import com.example.ppoketmonapi.service.BattleService;
import com.example.ppoketmonapi.service.PocketService;
import com.example.ppoketmonapi.service.ScoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/battle")
public class BattleController {
    final BattleService battleService;
    final PocketService pocketService;
    final ScoreService service;

    @PostMapping("battle-join/{id}")
    public CommonResult setBattle(@PathVariable long id) throws Exception {
        PocketMon pocketMon = pocketService.getData(id);
        battleService.setBattle(pocketMon);

        CommonResult response = new CommonResult();
        response.setMsg("출전 완료");
        response.setCode(0);

        return response;
    }

    @DeleteMapping("battle/del/stage-{id}")
    public CommonResult delMonster(@PathVariable long id){
        battleService.delMonster(id);

        CommonResult result = new CommonResult();
        result.setMsg("삭제되었습니다.");
        result.setCode(0);
        return result;
    }

    @GetMapping("battle/item/{claasNo}")
    public List<ScoreItem> getScore(@PathVariable int claasNo){
        return service.getClassAVG(claasNo);
    }
}
