package com.example.ppoketmonapi.controller;

import com.example.ppoketmonapi.model.CommonResult;
import com.example.ppoketmonapi.service.TempService;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    @PostMapping("/upload")
    public String setRentCarFile(@RequestParam("csvFile") MultipartFile csvFile) throws IOException {
        tempService.setRentCarFile(csvFile);

        return "성공!";
    }
}
