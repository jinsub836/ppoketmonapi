package com.example.ppoketmonapi.controller;

import com.example.ppoketmonapi.model.*;
import com.example.ppoketmonapi.service.PocketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/pocket")
public class PocketController {
    private final PocketService pocketService;

    @PostMapping("/create")
    public CommonResult setPocketmon(@RequestBody PocketRequest request){
        CommonResult response = new CommonResult();

        pocketService.setPocketMon(request);
        response.setMsg("등록되었습니다.");
        response.setCode(0);
        return response;
    }

    @GetMapping("/all")
    public ListResult<PocketItem> getList(){
        List<PocketItem> list = pocketService.getPocketMon();

        ListResult<PocketItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setMsg("조회에 성공하였습니다.");
        response.setCode(0);

        return response;
    }

    @GetMapping("/detail/{id}")
    public DetailResult<PocketResponse> getSingleDetails(@PathVariable long id){
        PocketResponse result = pocketService.getPocketMons(id);

        DetailResult<PocketResponse> response = new DetailResult<>();
        response.setData(result);
        response.setMsg("상세 정보 입니다.");
        response.setCode(0);

        return response;
    }

    @PutMapping("/out/{id}")
    public String putPocketMon(@PathVariable long id){
        pocketService.putPocketMon(id);
        return "탈퇴 완료";
    }
}
