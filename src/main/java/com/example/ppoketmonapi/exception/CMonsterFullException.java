package com.example.ppoketmonapi.exception;

public class CMonsterFullException extends RuntimeException {
    CMonsterFullException(String msg, Throwable t) {
        super(msg, t);
    }

    CMonsterFullException(String msg) {
        super(msg);
    }

    public CMonsterFullException() {
        super();
    }
}
