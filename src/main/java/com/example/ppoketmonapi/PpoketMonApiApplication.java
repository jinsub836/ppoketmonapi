package com.example.ppoketmonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpoketMonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpoketMonApiApplication.class, args);
	}

}
