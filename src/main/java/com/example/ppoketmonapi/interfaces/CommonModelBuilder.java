package com.example.ppoketmonapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
